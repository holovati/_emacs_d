;; Minimal UI
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(tooltip-mode    -1)
(menu-bar-mode   -1)

;; Default config
(show-paren-mode t)

(setq make-backup-files nil)

(fset 'yes-or-no-p 'y-or-n-p)

(global-set-key (kbd "C-x k") 'kill-this-buffer)

(global-hl-line-mode +1)

(global-linum-mode +1)

(global-set-key [M-left] 'windmove-left)          ; move to left window
(global-set-key [M-right] 'windmove-right)        ; move to right window
(global-set-key [M-up] 'windmove-up)              ; move to upper window
(global-set-key [M-down] 'windmove-down)          ; move to lower window


;; Package configs
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("gnu"   . "http://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))
(package-initialize)

;; Bootstrap `use-package`
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;; Helm
(use-package helm
             :ensure t
             :init
             (setq helm-mode-fuzzy-match t)
             (setq helm-completion-in-region-fuzzy-match t)
             (setq helm-candidate-number-list 50)
             :bind
             ("C-c o" . helm-ls-git-ls)
             ("C-c p" . helm-git-grep)
             ("C-c p" . helm-git-grep-at-point)
	     ("M-x"   . helm-M-x)
	     ("C-s"   . helm-occur)
	     ("C-x b" . helm-buffers-list)
	     ("C-x C-f" . helm-find-files))
    
;; Which Key
(use-package which-key
	     :ensure t
	     :init
	     (setq which-key-separator " ")
	     (setq whichg-key-prefix-prefix "+")
	     :config
	     (which-key-mode))

;; git grep
(use-package helm-git-grep
	     :ensure t)

;; git ls
(use-package helm-ls-git
	     :ensure t
	     :config)

;; multiple cursors
(use-package multiple-cursors
	     :ensure t)

;; Theme
(use-package doom-themes
	     :ensure t
	     :config
	     (load-theme 'doom-one t)
	     (doom-themes-visual-bell-config))

;; Google c-style
(use-package google-c-style
	     :ensure t
	     :config
	     (add-hook 'c-mode-common-hook 'google-set-c-style)
	     (add-hook 'c-mode-common-hook 'google-make-newline-indent)
)
;; Rust mode
(use-package rust-mode
	     :ensure t
	     :config)

;; Clang format on save
(use-package clang-format
	     :ensure t
	     :config
	     (add-hook 'c-mode-common-hook (lambda () (add-hook 'before-save-hook 'clang-format-buffer))))
(put 'downcase-region 'disabled nil)
